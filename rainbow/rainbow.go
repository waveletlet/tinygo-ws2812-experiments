// For manipulating colors

package rainbow

import (
	"image/color"
)

type ColorParam struct {
	Val     uint8
	Min     uint8
	Max     uint8
	Reverse uint8
}

func ShiftColor(r, g, b *ColorParam) color.RGBA {
	r = ShiftComponent(r)
	g = ShiftComponent(g)
	b = ShiftComponent(b)

	return color.RGBA{R: r.Val, G: g.Val, B: b.Val}
}

func ShiftComponent(c *ColorParam) *ColorParam {
	// TODO do better than hardcoded changing by 10
	shiftBy := uint8(10)
	if c.Reverse == 0 {
		if c.Val < (255 - shiftBy) {
			c.Val = c.Val + shiftBy
		} else {
			c.Val = c.Max
		}
	} else {
		if (c.Val - shiftBy) < (255 - shiftBy) { //as in, if subtracting didn't roll over 0
			c.Val = c.Val - shiftBy
		} else {
			c.Val = c.Min
		}
	}

	if c.Val >= c.Max {
		c.Val = c.Max
		c.Reverse = 1
	} else if c.Val <= c.Min {
		c.Val = c.Min
		c.Reverse = 0
	}

	return c
}

func MakePalette(r, g, b *ColorParam, leng int) []color.RGBA {
	colors := make([]color.RGBA, leng)
	colors[0] = color.RGBA{R: r.Val, G: g.Val, B: b.Val}
	for i := 1; i < leng; i++ {
		colors[i] = ShiftColor(r, g, b)
	}
	return colors
}
