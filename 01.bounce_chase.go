// Connects to an WS2812 RGB LED strip with 60 LEDS.
package main

import (
	"image/color"
	"machine"
	"time"

	. "./chase"
	"tinygo.org/x/drivers/ws2812"
)

var leds int = 60
var pin machine.Pin = 6

func main() {
	pin.Configure(machine.PinConfig{Mode: machine.PinOutput})

	strp := ws2812.New(pin)
	for {
		Chase(color.RGBA{R: 0xcf, G: 0x0f, B: 0xcc}, 5, leds, strp, (100 * time.Millisecond))
		ChaseReverse(color.RGBA{R: 0x0f, G: 0xcc, B: 0xcc}, 5, leds, strp, (100 * time.Millisecond))
	}
}
