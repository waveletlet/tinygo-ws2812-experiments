// Connects to an WS2812 RGB LED strip with 60 LEDS.
package main

import (
	"machine"
	"time"

	. "./chase"
	"./rainbow"
	"tinygo.org/x/drivers/ws2812"
)

const (
	LEDs int   = 60
	R    uint8 = 66
	G    uint8 = 4
	B    uint8 = 12
)

var pin machine.Pin = 6

func main() {
	pin.Configure(machine.PinConfig{Mode: machine.PinOutput})
	strp := ws2812.New(pin)

	// Params: current val, min, max, reverse?
	r := &rainbow.ColorParam{R, 0, 150, 0}
	g := &rainbow.ColorParam{G, 0, 66, 0}
	b := &rainbow.ColorParam{B, 0, 12, 0}

	for {
		ChaseMulticolor(rainbow.MakePalette(r, g, b, LEDs), 35, LEDs, strp, (150 * time.Millisecond))
		ChaseReverse(rainbow.ShiftColor(r, g, b), 35, LEDs, strp, (150 * time.Millisecond))
	}
}
