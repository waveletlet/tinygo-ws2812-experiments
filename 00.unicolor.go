// Connects to an WS2812 RGB LED strip with 60 LEDS.
// Basic all LEDs on one color

package main

import (
	"image/color"
	"machine"

	"tinygo.org/x/drivers/ws2812"
)

var leds int = 60
var pin machine.Pin = 6
var r int = 66
var g int = 4
var b int = 12

func main() {
	pin.Configure(machine.PinConfig{Mode: machine.PinOutput})

	farbe := color.RGBA{R: uint8(r), G: uint8(g), B: uint8(b)}
	strp := ws2812.New(pin)
	lights := make([]color.RGBA, leds)
	for i := range lights {
		lights[i] = farbe
	}
	strp.WriteColors(lights[:])
}
