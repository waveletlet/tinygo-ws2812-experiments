package chase

import (
	"image/color"
	"time"

	"tinygo.org/x/drivers/ws2812"
)

var OFF color.RGBA = color.RGBA{R: 0x00, G: 0x00, B: 0x00}

func Chase(farb color.RGBA, tail int, leds int, strp ws2812.Device, speed time.Duration) {
	lights := make([]color.RGBA, leds)
	for i := 0; i < leds+tail; i++ {
		if i < leds {
			lights[i] = farb
		}
		if i >= tail {
			lights[i-tail] = OFF
		}
		strp.WriteColors(lights[:])
		time.Sleep(speed)
	}
}

func ChaseReverse(farb color.RGBA, tail int, leds int, strp ws2812.Device, speed time.Duration) {
	lights := make([]color.RGBA, leds)
	for i := leds; i >= -tail; i-- {
		if i < leds && i > -1 {
			lights[i] = farb
		}
		if i+tail < leds && i+tail > -1 {
			lights[i+tail] = OFF
		}
		strp.WriteColors(lights[:])
		time.Sleep(speed)
	}
}

func ChaseMulticolor(colors []color.RGBA, tail int, leds int, strp ws2812.Device, speed time.Duration) {
	if len(colors) < leds {
		repeated := make([]color.RGBA, leds)
		endIndex := copy(repeated, colors)
		for endIndex < len(repeated) {
			copy(repeated[endIndex:], repeated[:endIndex])
			endIndex *= 2
		}
		colors = repeated
	}

	lights := make([]color.RGBA, leds)
	for i := 0; i < leds+tail; i++ {
		if i < leds {
			lights[i] = colors[i]
		}
		if i >= tail {
			lights[i-tail] = OFF
		}
		strp.WriteColors(lights[:])
		time.Sleep(speed)
	}

}
